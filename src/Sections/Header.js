import React, { Component } from "react";
import Fade from "react-reveal";
import { MenuIcon } from "@heroicons/react/solid"
import NextSection from "../Components/NextSection";

export default class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showMenu: false
    }
  }

  toggleMenu = () => {
    this.setState({
      showMenu: !this.state.showMenu
    })
  }

  render() {
    const { links } = this.props
    const { showMenu } = this.state
    return (
      <header>

        <nav
          id="nav-wrap"
          className="
      flex 
      flex-wrap 
      w-full 
      justify-between 
      bg-gradient-to-b 
      from-black 
      via-black 
      py-4 
      px-4 
      lg:py-0
      ">
          <a className="text-primary" href="#home">
            Ashes of Arcadia logo goes here
          </a>
          <MenuIcon
            className="w-6 h-6 text-secondary lg:hidden block cursor-pointer z-20"
            onClick={this.toggleMenu} />

          <Fade right>
            <div className={`${!showMenu ? 'hidden' : ''} lg:flex flex-col items-end px-5 fixed bg-black right-0 py-5 z-10 lg:relative lg:bg-transparent lg:self-end text-right`}
              id="menu">
              <ul id="nav" className="pt-4 lg:flex lg:justify-between lg:pt-0 lg:flex-row lg:self-end">
                {links.map(link => {
                  return (
                    <li className="">
                      <a className="lg:p-4 py-2 block text-secondary" href={`#${link.url}`} onClick={this.toggleMenu}>
                        {link.title.toUpperCase()}
                      </a>
                    </li>
                  )
                })}
              </ul>

            </div>
          </Fade>

        </nav>

        <Fade bottom>
          <div className="w-full text-center my-20 lg:px-40 px-10">
            <div className="text-primary text-center flex-1">
              <h1 className="text-8xl mt-40 mb-0">Ashes of Arcadia</h1>
              <h3 className="text-secondary text-4xl mb-10">Play & Earn!</h3>
              <hr className="w-1/4 m-auto text-tertiary" />
            </div>
          </div>
        </Fade>

        <NextSection
          nextSectionUrl="about" />
      </header>
    );
  }
}
