import React, { Component } from "react";
import Fade from "react-reveal";

export default class Roadmap extends Component {
  render() {

    return (
      <section id="roadmap" className="bg-gray text-secondary">
        <Fade duration={1000}>
          <div className="row">
            <div className="w-full m-auto text-center">
              <p className="text-xl">Roadmap</p>
            </div>
          </div>
        </Fade>
      </section>
    );
  }
}
