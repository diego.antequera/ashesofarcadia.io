import React, { Component } from "react";
import Fade from "react-reveal";

export default class ArcadiaToken extends Component {
  render() {

    return (
      <section id="arcadia-token" className="bg-primary text-gray">
        <Fade duration={1000}>
          <div className="row">
            <div className="w-full m-auto text-center">
              <p className="text-xl">Tokenomics</p>
            </div>
          </div>
        </Fade>
        <Fade duration={1000}>
          <div className="row">
            <div className="w-full m-auto text-center">
              <p className="text-xl">Contracts</p>
            </div>
          </div>
        </Fade>
        <Fade duration={1000}>
          <div className="row">
            <div className="w-full m-auto text-center">
              <p className="text-xl">KYC</p>
            </div>
          </div>
        </Fade>
      </section>
    );
  }
}
