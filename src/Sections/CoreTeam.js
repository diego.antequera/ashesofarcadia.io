import React, { Component } from "react";
import Fade from "react-reveal";

export default class CoreTeam extends Component {
  render() {

    return (
      <section id="arcadia-token" className="bg-primary text-gray">
        <Fade duration={1000}>
          <div className="row">
            <div className="w-full m-auto text-center">
              <p className="text-xl">Core Team</p>
            </div>
          </div>
        </Fade>
      </section>
    );
  }
}
