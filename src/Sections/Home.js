import React, { Component } from "react";
import Fade from "react-reveal";

export default class Home extends Component {
  render() {

    return (
      <section id="home" className=" text-secondary">
        <Fade duration={1000}>
          <div className="row">
            <div className="w-full m-auto text-center">
              <p className="text-xl">Embark in this brutal adventure to try and save what's left of the once beautiful lands of Arcadia</p>
              <br />
              <p>Gather your heroes, sharpen their blade, hone in their skills, and lay waste to your enemies!</p>
            </div>
          </div>
        </Fade>
      </section>
    );
  }
}
