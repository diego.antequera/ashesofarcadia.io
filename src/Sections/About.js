import React, { Component } from "react";
import Fade from "react-reveal";

export default class About extends Component {
  render() {

    return (
      <section id="about" className=" text-secondary">
        <Fade duration={1000}>
          <div className="row">
            <div className="w-full m-auto text-center">
              <p className="text-xl">What is Ashes of Arcadia?</p>
            </div>
          </div>
        </Fade>
        <Fade duration={1000}>
          <div className="row">
            <div className="w-full m-auto text-center">
              <p className="text-xl">Arcadia - The World</p>
            </div>
          </div>
        </Fade>
        <Fade duration={1000}>
          <div className="row">
            <div className="w-full m-auto text-center">
              <p className="text-xl">Heroes</p>
            </div>
          </div>
        </Fade>
        <Fade duration={1000}>
          <div className="row">
            <div className="w-full m-auto text-center">
              <p className="text-xl">Weapons</p>
            </div>
          </div>
        </Fade>
        <Fade duration={1000}>
          <div className="row">
            <div className="w-full m-auto text-center">
              <p className="text-xl">Skills</p>
            </div>
          </div>
        </Fade>
      </section>
    );
  }
}
