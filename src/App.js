import React, { Component } from "react";
import ReactGA from "react-ga";
import $ from "jquery";
import Header from "./Sections/Header";
import Home from "./Sections/Home";
import About from "./Sections/About";
import Ashes from "./Components/Ashes";
import ArcadiaToken from "./Sections/ArcadiaToken";
import Roadmap from "./Sections/Roadmap"
import CoreTeam from "./Sections/CoreTeam"

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }

    ReactGA.initialize("G-5WC1KYRQ9D");
    ReactGA.pageview(window.location.pathname);
  }

  render() {
    const links = [
      {
        url: "theworld",
        title: "The World"
      },
      {
        url: "heroes",
        title: "Heroes"
      },
      {
        url: "weapons",
        title: "Weapons"
      },
      {
        url: "skills",
        title: "skills"
      },
      {
        url: "roadmap",
        title: "Roadmap"
      },
      {
        url: "whitepaper",
        title: "Whitepaper"
      },
      {
        url: "partners",
        title: "Partners"
      },
      {
        url: "coreTeam",
        title: "Core Team"
      },
    ]
    return (
      <div className="App font-body">
        <Ashes />
        <Header
          links={links}
        />
        <Home />
        <About />
        <ArcadiaToken />
        <Roadmap />
        <CoreTeam />
      </div>
    );
  }
}

export default App;
