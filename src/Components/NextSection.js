import React, { Component } from "react";
import Fade from "react-reveal";
import { ChevronDoubleDownIcon, ChevronDownIcon } from "@heroicons/react/solid"

export default class NextSection extends Component {
  render() {

    return (
      <div className="transition rounded-full absolute bottom-10 left-1/2 w-10 -ml-5 text-tertiary duration-500 hover:scale-125 hover:bg-tertiary hover:text-gray">
        <a
          className="w-10 smoothscroll"
          href={`#${this.props.nextSectionUrl}`}>
          <ChevronDoubleDownIcon className="h-10 w-10 m-auto" />
        </a>

      </div>
    );
  }
}
