import React, { Component } from "react";
import ParticlesBg from "particles-bg";

export default class Ashes extends Component {
  render() {
    const config = {
      num: [4, 7],
      rps: 0.5,
      radius: [1, 40],
      life: [1.5, 3],
      v: [2, 3],
      tha: [-40, 40],
      alpha: [0.2, 0.05],
      scale: [0.05, 0.005],
      position: "all", // all or center or {x:1,y:1,width:100,height:100}
      color: ["#888", "#000", "#fff"],
      cross: "cross", // cross or bround
      random: 15,  // or null,
      g: 1,    // gravity
      f: [2, -1], // force
      onParticleUpdate: (ctx, particle) => {
        ctx.beginPath();
        ctx.arc(particle.p.x, particle.p.y, particle.radius * 2, 0, 360);
        ctx.fillStyle = particle.color;
        ctx.fill();
        ctx.closePath();
      }
    }
    return (
      <ParticlesBg type="custom" config={config} bg={true} />
    )
  }
}