module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false,
  theme: {
    extend: {},
    stroke: {

    },
    container: {
      center: true
    },
    colors: {
      transparent: 'transparent',
      primary: "#fe6928",
      secondary: "#eee",
      tertiary: "#888",
      gray: "#333",
      black: "#000",
      red: "#f00"
    },
    fontFamily: {
      body: ['Marcellus', 'ui-serif']
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
